wx - the erlang binding of wxWidgets
-------------
> Biblioteka wx(Erlang), czyli port wxWidgets dla Erlanga - tworzenie graficznego interfejsu użytkownika w Erlangu

> Prezentacja wykonana w ramach przedmiotu "Programowanie w języku Erlang" prowadzonego na II roku kierunku Informatyka na Wydziale Informatyki, Elektroniki i Telekomunikacji Akademii Górniczo-Hutniczej w Krakowie.

> Autorzy: Jakub Łabuz, Mariusz Wojakowski

> Kraków 2014