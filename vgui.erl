-module(vgui).
-include_lib("wx/include/wx.hrl").
-behaviour(wx_object).
-compile(export_all).

%% API
-export([]).

start() ->
  Server = wx:new(),
  Frame = wxFrame:new(Server, -1, "Frame", [{size, {180, 150}}]),
  Config = [{parent, Frame}],
  wx_object:start_link(?MODULE, Config, []).

init(Config) ->
  wx:batch(fun() -> do_init(Config) end).

do_init(Config) ->
  Parent = proplists:get_value(parent, Config),
  Panel = wxPanel:new(Parent, []),

  OuterSizer = wxBoxSizer:new(?wxHORIZONTAL),
  MainSizer = wxBoxSizer:new(?wxVERTICAL),
  TextSizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, [{label, "Wprowadz liczbe: "}]),
  TextCtrl = wxTextCtrl:new(Panel, ?wxID_ANY),
  StaticText = wxStaticText:new(Panel, ?wxID_ANY, "Wynik: ", []),
  Button = wxButton:new(Panel, ?wxID_ANY, [{label, "Licz!"}]),

  wxSizer:add(TextSizer, TextCtrl, []),

  wxSizer:addSpacer(MainSizer, 5),
  wxSizer:add(MainSizer, TextSizer),



  wxSizer:addSpacer(MainSizer, 5),
  wxSizer:add(MainSizer, Button),
  wxSizer:addSpacer(MainSizer, 5),
  wxSizer:add(MainSizer, StaticText),
  wxSizer:addSpacer(OuterSizer, 20),
  wxSizer:add(OuterSizer, MainSizer),


  wxPanel:setSizer(Panel, OuterSizer),
  wxFrame:show(Parent),

  wxPanel:connect(Panel, command_button_clicked),

  {Panel, {Button, TextCtrl, StaticText}}.



handle_event(#wx{event=#wxCommand{type=command_button_clicked}}, State) ->
  {_Button, TextCtrl, StaticText} = State,
  io:format("Zlapano event! ~n"),
  io:format("Wprowadzono: ~s~n", [wxTextCtrl:getValue(TextCtrl)]),
  io:format("------------- ~n"),
  case (catch list_to_integer(wxTextCtrl:getValue(TextCtrl))) of
    {'EXIT', _} -> wxStaticText:setLabel(StaticText, "Zla wartosc!");
    Wartosc -> wxStaticText:setLabel(StaticText, "Wynik: " ++ integer_to_list(volume(Wartosc)))
  end,

  {noreply, State}.

handle_call(Request, From, State) ->
  erlang:error(not_implemented).

handle_cast(Request, State) ->
  erlang:error(not_implemented).

handle_info(Info, State) ->
  erlang:error(not_implemented).

terminate(Reason, State) ->
  ok.

code_change(OldVsn, State, Extra) ->
  erlang:error(not_implemented).


volume(Edge) ->
  Edge*Edge*Edge.